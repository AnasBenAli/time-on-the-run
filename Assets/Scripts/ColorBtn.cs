﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBtn : MonoBehaviour
{
    public Sprite init_sprite;
    public Sprite lightened_sprite;
    public SpriteRenderer sr;
    public GameManager gm;
    public ColorMemo cm;
    private AudioSource As;
    public AudioClip beep;
    public int Btn_index;
    private void Awake()
    {
        As = GetComponent<AudioSource>();
    }
    /// <summary>
    /// Lights up the button.
    /// </summary>
    public void light_up()
    {
        sr.sprite = lightened_sprite;
        As.PlayOneShot(beep);
    }
    /// <summary>
    /// Lights down the button. 
    /// </summary>
    public void light_down()
    {
        sr.sprite = init_sprite;
    }
    /// <summary>
    /// Lights up the button for 1 second approximitaly.
    /// </summary>
    public void animate_btn()
    {
        light_up();
        Invoke("light_down",0.5f);
    }
    private void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            Vector3 touch_pos = gm.GetPositionThroughtScreen(touch);
            if (gm.IsClose(gameObject.transform.position, touch_pos,0.6f))
            {

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        if (!cm.in_sequence_animation || cm.WaitForPlayerInput)
                        {
                            light_up();
                        }
                        break;
                    case TouchPhase.Ended:
                        if (!cm.in_sequence_animation || cm.WaitForPlayerInput)
                        { 
                            light_down();
                            cm.input_sequence.Add(Btn_index);
                            cm.WaitForPlayerInput = true;
                            cm.animate_sequence();
                            cm.resetAllBtns();
                        }
                            
                        
                        break;
                    case TouchPhase.Moved:
                        if (!cm.in_sequence_animation || cm.WaitForPlayerInput)
                        {
                            light_down();
                            cm.resetAllBtns();
                        }
                        break;

                
                                
                }
            }
        }
    }
}
