﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGame : MonoBehaviour
{
    protected const int BaseScore=500;
    [Range(1, 5)]
    public GameManager gm;
    protected bool MiniGameEnded = false;
    public int CalculateScore()
    {
        return BaseScore * gm.currentDifficulty;
    }//Calculer Score
    public virtual void ResetMiniGame()
    {
        
    }//Reset Minigame
    public virtual void Disable()
    {
        gameObject.SetActive(false);
    }//Activate MiniGame
    public virtual void Enable()
    {
        gameObject.SetActive(true);
    } //Desactiver MiniGame
    public virtual void NewSQS()
    {

    }
}
