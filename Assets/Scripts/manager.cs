﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manager : MonoBehaviour
{

    public GameObject[] levels;
    int currentlevel;

    public void correctAnswer()
    {
        if (currentlevel + 1 != levels.Length)
        {
            levels[currentlevel].SetActive(false);
            currentlevel++;
            levels[currentlevel].SetActive(true);
        }

    }
}
