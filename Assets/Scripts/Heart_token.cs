﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart_token : MonoBehaviour
{
    public GameManager gm;
    private Rigidbody2D rb;
    private Vector3 init;
    // Start is called before the first frame update
    private void Awake()
    {
        init = gameObject.transform.position;
        rb = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        runToTheLeft();
        foreach (Touch touch in Input.touches)
        {
            Vector3 touch_pos = gm.GetPositionThroughtScreen(touch);
            if (gm.IsClose(gameObject.transform.position, touch_pos, 0.6f))
            {
                gm.heal();
                Reset(false);
                 gm.Activate_random_event();
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.layer == 8)
        {
            gm.heal();
            Reset(false);
            gm.Activate_random_event();
        }
    }
    void runToTheLeft()
    {
      rb.velocity = -1 * (Vector3.right + new Vector3(0.0f, Mathf.Sin(Time.time * 10), 0.0f));
    }
    public void Reset(bool mode)
    {
        if (gameObject.activeSelf)
        {
            gameObject.transform.position = init;
            gameObject.SetActive(mode);
        }
    }
}
