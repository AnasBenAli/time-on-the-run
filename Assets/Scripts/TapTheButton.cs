﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapTheButton : MiniGame
{

    public GameObject Liquid;
    private SpriteRenderer spr;
    private Sprite sp;
    public Sprite NewSprite;

    
    private bool isPressed;
    private bool wasPressed;
    private bool isHeld;
    private bool isTapped;

    private float riseStep = 0.3f;
    private float fallStep = 0.01f;
    private float initLiquidHeight;
    private float MinLiquidHeight=0.1f;
    private float MaxLiquidLevel = 4.8f;
    
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponentInChildren<SpriteRenderer>();
        sp = spr.sprite;
        isPressed = false;
        wasPressed = false;
        isHeld = false;
        initLiquidHeight = Liquid.transform.localScale.y;
        isTapped = false;
        
    }

    private void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        foreach (Touch touch in Input.touches)
            {
                        
                Vector3 touch_pos = gm.GetPositionThroughtScreen(touch); //Returns the position in of touch on the screen
                if (gm.IsClose(gameObject.transform.position, touch_pos, 0.4f)) //Checks if touch is close to this.gameObject
                {
                    isPressed = true;
                    
            }
            }
            if (isPressed)// Checks if the button was presssed this frame
            {
            if (!wasPressed)
            {
                isTapped = true;
            }
                if (wasPressed) // Checks if it was pressed last frame
                {
               // GameManager.PlaySound("key_release");
                isHeld = true; // If the button was pressed last frame and this frame it means it was held
                }
                wasPressed = true; // A way to confirm to the next frame that is was pressed
                spr.sprite = NewSprite;

            }
            else if (!isPressed && spr.sprite != sp)  // Checks if it wasn't pressed this frame and if the Sprite is not the default one
            {                                       // A way to optimise sprite rendering
                spr.sprite = sp;
            }
            else if (wasPressed && !isPressed) //If the button is not held and was,
            {
                wasPressed = false;
            }

            if (!isPressed)
            {
            bumpDownMeter();
            
            isHeld = false;
            }
            if (isHeld)
            {
            bumpDownMeter();
            }
            if (isTapped)
            {
            
                bumpUpMeter();
            }
        isPressed = false;
        isTapped = false;

        if (MiniGameEnded)
        {
            gm.addToScore(CalculateScore());
            gm.DisplayScore();
            CloseMinigame();

        }
    }
    private void bumpUpMeter()
    {
        if (!gm.paused)
        {
            GameManager.PlaySound("random_key_press");
            float Step = riseStep / (gm.currentDifficulty * 0.4f);
            if (Liquid.transform.localScale.y + Step >= MaxLiquidLevel)
            {
                Liquid.transform.localScale = new Vector3(Liquid.transform.localScale.x, MaxLiquidLevel, Liquid.transform.localScale.z);
                MiniGameEnded = true;
            }
            else if (Liquid.transform.localScale.y < MaxLiquidLevel)
                Liquid.transform.localScale = new Vector3(Liquid.transform.localScale.x, Liquid.transform.localScale.y + Step, Liquid.transform.localScale.z);
        }
       
    }
    private void bumpDownMeter()
    {
        if (!gm.paused) {
            if (Liquid.transform.localScale.y <= MinLiquidHeight)
            {
                gm.damage();
                CloseMinigame();
            }
            if (Liquid.transform.localScale.y > MinLiquidHeight)
                Liquid.transform.localScale = new Vector3(Liquid.transform.localScale.x, Liquid.transform.localScale.y - fallStep, Liquid.transform.localScale.z);
        }
        
    }

    public override void ResetMiniGame()
    {
        if (gameObject.activeSelf)
        {
            this.Liquid.transform.localScale = new Vector3(this.Liquid.transform.localScale.x, initLiquidHeight, this.Liquid.transform.localScale.z);
            this.gameObject.SetActive(false);
        }
    }
    public void CloseMinigame()
    {
        gm.inMiniGame = false;
        gm.Activate_random_event();
        ResetMiniGame();
        MiniGameEnded = false;
    }
    public override void Disable()
    {
        gameObject.SetActive(false);
    }//Activer MiniGame
    public override void Enable()
    {
        gameObject.SetActive(true);
    } 
}
