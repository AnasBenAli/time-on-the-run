﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoTiles : MiniGame
{
    public int number_of_taps = 0;
    public List<tile> Tiles;

    public override void ResetMiniGame()
    {
        if (gameObject.activeSelf)
        { 
            foreach (tile t in Tiles)
            {
                t.ResetTile(true);
                t.number_of_taps = 0;
            }
            gameObject.SetActive(false);
        }
       
    }

}
