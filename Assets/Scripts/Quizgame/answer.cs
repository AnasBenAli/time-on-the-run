﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class answer : MonoBehaviour
{
    public bool iscorrect = false;
    public manager2 quizmanager;

   public void answers()
    {
        if (iscorrect)
        {
        Debug.Log("Correct Answer");
            quizmanager.correct();
        }
        else
        {
            Debug.Log("Wrong Answer");
            quizmanager.wrong();
        }
    }
}
