﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class manager2 : MonoBehaviour
{
    public List<qstanswer> QnA;
    public GameObject[] options;
    public int currentQuestion;
    
    public Text Questiontxt;
    public Text scoretxt;

    public int score;
    int totalqst = 0;

    public GameObject quizpanel;
    public GameObject finishpanel;

    private void Start()
    {
        score = QnA.Count; // score
        totalqst = QnA.Count; // nmbre de qsts
        finishpanel.SetActive(false);
        generateqst();
    }
    void generateqst()
    {
        if (QnA.Count > 0)
        {
            currentQuestion = Random.Range(0, QnA.Count);
            Questiontxt.text = QnA[currentQuestion].question;
            setanswers();

        }
        else
        {
            Debug.Log("Out of Questions");
            gameover();
        }
    }
    // les reponses
    void setanswers()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<answer>().iscorrect = false;
            options[i].transform.GetChild(0).GetComponent<Text>().text = QnA[currentQuestion].answers[i];

            if (QnA[currentQuestion].correctanswer == i + 1)
            {
                options[i].GetComponent<answer>().iscorrect = true;
            }
        }
    }
    //if answer if correct
    public void correct()
    {
      
        QnA.RemoveAt(currentQuestion);
        generateqst();
    }
    //if answer is wrong
    public void wrong()
    {
        score -= 1;
        QnA.RemoveAt(currentQuestion);
        generateqst();
    }
    //generate qst

    public void gameover()
    {
        quizpanel.SetActive(false);
        finishpanel.SetActive(true);
        scoretxt.text ="Score :"+ score + "/" + totalqst;
    }
    public void retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
   
}
