﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindThePair : MiniGame
{
    public List<Card> Cards;
    public List<Sprite> front_sprites;
    public static int chosen_cardIndex = -1;
    public static int revealed_cardIndex = -1;

    private void Update()
    {
        CheckSamePair();
        if(AllRevealed())
        {
            Invoke("closeGame",0.5f);
            gm.addToScore(CalculateScore());
        }
    }
    private void Awake()
    {
        shuffle();
    }
    /// <summary>
    /// Sets the game to false, disabling it.
    /// </summary>
    public void closeGame()
    {
        ResetThisMinigame(false);
        gm.token.RestartToken();
    }
    /// <summary>
    /// Checks if the both chosen cards have the same picture
    /// </summary>
    /// <returns>Returns false of they're not the same</returns>
    public bool CheckSamePair()
    {
            if(chosen_cardIndex != -1 && revealed_cardIndex != -1)
            { 
                if(Cards[chosen_cardIndex].front == Cards[revealed_cardIndex].front && revealed_cardIndex != chosen_cardIndex)
                {
                    LockPairs(Cards[chosen_cardIndex]);
                    chosen_cardIndex = -1;
                    revealed_cardIndex = -1;
                    return true;
                }
                else
                {
                Cards[chosen_cardIndex].delayedFlip();
                Cards[revealed_cardIndex].delayedFlip();
                chosen_cardIndex = -1;
                revealed_cardIndex = -1;
                }
            }
     return false;
    }

    /// <summary>
    /// Disables all cards of the same picture, keeping the player from interacting with them.
    /// </summary>
    /// <param name="card"></param>
    private void LockPairs(Card card)
    {
        for(int i=0;i<Cards.Count;i++)
        {
            if(Cards[i].front == card.front)
            {
                Cards[i].locked = true;
            }
        }
    }

    /// <summary>
    /// Checks if all cards are revealed.
    /// </summary>
    /// <returns>Returns true if they are all revealed</returns>
    public bool AllRevealed()
    {
        foreach(Card c in Cards)
        {
            if (!c.isRevealed())
                return false;
        }
        return true;
    }
    /// <summary>
    /// Rearanges all cards in a random order.
    /// </summary>
    private void shuffle()
    {
        System.Random r = new System.Random();
        foreach (Card c in Cards)
        {
            int num = r.Next(0, 8);
            Vector3 container = new Vector3();
            container = c.gameObject.transform.position;
            c.gameObject.transform.position= Cards[num].gameObject.transform.position;
            Cards[num].gameObject.transform.position = container;
        }
    }

    public void ResetCards()
    {
         foreach(Card card in Cards)
        {
            card.gameObject.SetActive(true);
            card.sr.sprite = card.back;
            card.locked = false;
        }
    }
    public void ResetThisMinigame(bool mode)
    {
        ResetCards();
        shuffle();
        chosen_cardIndex = -1;
        revealed_cardIndex = -1;
        gameObject.SetActive(mode);
    }
}
