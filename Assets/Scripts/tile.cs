﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tile : MonoBehaviour
{
    [Range(-5f, 5f)]
    public float speed = 2f;
    Rigidbody2D rb;
    private float offset = 0;
    public PianoTiles parent;
    Vector3 initpos;
    Color color;
    System.Random r = new System.Random();
    int num; //A period of time to wait
    public int number_of_taps = 0;
    public GameManager gm;
    public AudioClip note;
    private void Awake()
    {
        
        rb = gameObject.GetComponent<Rigidbody2D>();
        initpos = gameObject.transform.position;

        color = gameObject.GetComponentInChildren<SpriteRenderer>().color; //Get's the value of the color
        color.a = 0.0f; // set's the alpha variable to 0
        gameObject.GetComponentInChildren<SpriteRenderer>().color = color; //Set's the aplha value of the color to 0
        randomAppearance();
        num = r.Next(0, 5);
    }
    private void FixedUpdate()
    {
        num = r.Next(0, 5); //sets a new random number
    }
    private void Update()
    {
        offset += (Time.deltaTime * speed) / 75f;
        rb.velocity = new Vector2(speed * offset * 1.2f, 0) * -1;
        color.a = offset * 5;
        gameObject.GetComponentInChildren<SpriteRenderer>().color = color;
        if (number_of_taps >= gm.currentDifficulty * 2) //checks the number of taps and desactivates
                                                        //if the player reached the amount of taps required
        {
            parent.ResetMiniGame();
            gm.Activate_random_event();
            gm.addToScore(parent.CalculateScore());
        }
        //DeadlyTouch();
    }
    /// <summary>
    /// Manages the player's touch input
    /// </summary>
    private void DeadlyTouch()
    {
        foreach (Touch touch in Input.touches)
        {
            Vector2 touch_pos = GameManager.GetPositionThroughtScreenStatic(touch, gameObject); //Get's the player's touch position
            if (GameManager.IsCloseStatic(gameObject.transform.position, touch_pos, 0.4f))//Checks if the player cliked the tile
            {
                GameManager.PlaySound("randomNote");
                number_of_taps++;
                gameObject.SetActive(false);
                randomAppearance();//resets the tile after a random period of time
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        DeadlyTouch();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        gameObject.SetActive(false);
        randomAppearance();
    }

    /// <summary>
    /// Resets the tile's position, offset and activates it
    /// </summary>
    public void ResetTile()
    {
        gameObject.transform.position = initpos;
        offset = 0;
        gameObject.SetActive(true);
    }
    public void ResetTile(bool mode)
    {
        gameObject.transform.position = initpos;
        offset = 0;
        gameObject.SetActive(mode);
    }

    /// <summary>
    /// Resets the tile after a delayed amount of time
    /// </summary>
    public void randomAppearance()
    {
        Invoke("ResetTile", num);
    }
}
