﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    public Sprite back;
    public Sprite front;
    public SpriteRenderer sr;
    public GameManager gm;
    public int CardIndex = -1;
    public GameObject Parent;
    

    public bool locked=false;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        reveal();
    }
    /// <summary>
    /// Flips the card.
    /// </summary>
    public void flip()
    {
        if(!locked)
        {
            if (sr.sprite == back)
                sr.sprite = front;
            else
                sr.sprite = back;
        }
    }

    /// <summary>
    /// Flips the card with a delay.
    /// </summary>
    public void delayedFlip()
    {
        Invoke("flip",0.5f);
    }

    /// <summary>
    /// Checks if the card is facing up.
    /// </summary>
    /// <returns>Returns true if it's facing up.</returns>
    public bool isRevealed()
    {
        if (sr.sprite == front)
            return true;
        else return false;
    }
 
    /// <summary>
    /// Takes the player's input and flips the card pressed if it's not locked.
    /// </summary>
    public void reveal()
    {
        if(!locked)
        { 
        foreach (Touch touch in Input.touches)
        {
            Vector3 touch_pos = gm.GetPositionThroughtScreen(touch);
                
            if (gm.IsClose(gameObject.transform.position, touch_pos, 0.3f))
            {
                switch (touch.phase)
                {
                    case TouchPhase.Ended:
                            if (FindThePair.revealed_cardIndex == -1)
                        {
                            FindThePair.revealed_cardIndex = CardIndex;
                        }
                        else
                        {
                            FindThePair.chosen_cardIndex = CardIndex;
                        }
                        flip();
                        break;
                }
            }
        }
        }
    }
}
