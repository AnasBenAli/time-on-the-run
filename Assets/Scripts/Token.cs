﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    public GameManager gm;
    private Vector3 initPos;
    public GameObject TokenSpriteGameObject;
    //Indexes: 0=Tap The Button, 1=Color Memo
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initPos = gameObject.transform.position;
    }

    void Update()
    {
        if (gameObject.activeSelf)
        {
            gm.inMiniGame = false;
        }
        else
            gm.inMiniGame = true;
        runToTheLeft();
        foreach(Touch touch in Input.touches)
        {
            Vector3 touch_pos = gm.GetPositionThroughtScreen(touch);

            if (gm.IsClose(gameObject.transform.position, touch_pos, 0.6f))
            {
                GameManager.PlaySound("pop");
                TriggerMinigame();
            }

        }
    }
    void runToTheLeft()
    {
        if (!gm.paused)
        {
            rb.velocity =-1* (Vector3.right + new Vector3(0.0f, Mathf.Sin(Time.time*10), 0.0f));
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            GameManager.PlaySound("pop");
            TriggerMinigame();
            
        }
    }
    public void ResetToken()
    {
        if (gameObject.activeSelf)
        {
            gameObject.transform.position = initPos;
        }
        
    }
    public void RestartToken()
    {
        if (gameObject.activeSelf)
        {
            ResetToken();
            Enable();
        }
       
    }
    public void Enable()
    {
        gameObject.SetActive(true);
    }
    public void Disable()
    {
        gameObject.SetActive(false);
    }
    
    private void TriggerMinigame()
    {
        gm.setRandomGame();
        gm.currentDifficulty = 1 + (gm.score / 10000);
        ResetToken();
        if (gm.current_minigame_index == 1)
        {
            gm.Minigames[1].NewSQS();
        }
        gm.Minigames[gm.current_minigame_index].Enable();
        Debug.Log("Current Difficulty: " +(1+ gm.score /10000));
        Disable();
    }
}
