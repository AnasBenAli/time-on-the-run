﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemybhv : MonoBehaviour
{
    public GameManager gm;
    private Vector2 initPos;
    [Range(-5f, 5f)]
    public float speed = 2f;
    private float offset=0;
    int touches = 0;
    int tapsRequired=3;
    public GameObject hint;
    Rigidbody2D rb;
    // Start is called before the first frame update

    private void Awake()
    {
        initPos = gameObject.transform.position;
        rb = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        offset += (Time.deltaTime * speed) / 10f;
        rb.velocity = new Vector2(speed * offset, 0) * -1;
        if(!gm.paused)
        {
            foreach (Touch touch in Input.touches)
            {
                Vector2 touch_pos = gm.GetPositionThroughtScreen(touch);
                if (Vector2.Distance(touch_pos, gameObject.transform.position) <= 0.4f)
                {
                    //Reset(false);
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            // GetComponent<Animator>().SetTrigger("Squash");
                            GetComponent<Animator>().SetTrigger("Flash");
                            GameManager.PlaySound("squish");
                            touches++;
                            if (touches >= tapsRequired * gm.currentDifficulty)
                            {
                                hint.SetActive(false);
                                Reset(false);
                                touches = 0;
                                gm.addToScore(tapsRequired * 500 - 100);
                                gm.Activate_random_event();
                            }
                            break;
                    }
                }
            }
        }

    
    }
    public void Reset(bool mode)
    {
        if (gameObject.activeSelf)
        {
            gameObject.transform.position = initPos;
            gameObject.SetActive(mode);
            offset = 0;
        }
    }
    public void falseReset()
    {
        if (gameObject.activeSelf)
        {
            gameObject.transform.position = initPos;
            gameObject.SetActive(false);
            offset = 0;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            gm.damage();
            falseReset();
            gm.Activate_random_event();
        }
    }

}
