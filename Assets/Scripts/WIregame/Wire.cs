﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wire : MonoBehaviour
{
    private Touch touch;
    private float speedmod;

    Vector3 startPoint;

    // Start is called before the first frame update
    void Start()
    {
        startPoint = transform.parent.position;
        speedmod = 0.01f;
    }
   

    private void Update()
    {
       if(Input.touchCount>0)
        {
            touch = Input.GetTouch(0);
            if(touch.phase== TouchPhase.Moved)
            {
                 Vector3 newposition=transform.position = new Vector3 (
                    transform.position.x + touch.deltaPosition.x * speedmod,
                    transform.position.y,
                     transform.position.z + touch.deltaPosition.y * speedmod);

                Vector3 direction = newposition - startPoint;
                transform.right=direction;
            }
        } 
    }

   
}
