﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public GameManager gm;
    public void pause_unpause()
    {
        if (gm.paused)
        {
            gm.paused = false;
        }
        else
            gm.paused = true;
    }
}
