﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ColorMemo : MiniGame
{
    public List<ColorBtn> Btns;
    public List<int> pressed_sequence;
    public List<int> sequence;
    public List<int> input_sequence;
    public GameObject blanket;
    public GameObject Check;
    public GameObject Cross;
    public bool in_sequence_animation=false;
    public bool WaitForPlayerInput=false;
    

    int count=0;
    int time_counter = 0;
    int input_timer = 0;
    private void FixedUpdate()
    {
        if (!gm.paused)
        {
            if (in_sequence_animation && !WaitForPlayerInput)
            {
                trigger_sequence();
            }
            else
            {
                RecordPlayerInputs();
            }
        }
    }
    private void Awake()
    {
        ResetMiniGame();
        this.gameObject.SetActive(true);
        startSequence();
    }
    /// <summary>
    /// Resets all of the attributes and sets the state to false.
    /// </summary>
    public override void ResetMiniGame()
    {
        if (gameObject.activeSelf)
        {
            WaitForPlayerInput = false;
            count = 0;
            time_counter = 0;
            input_timer = 0;
            in_sequence_animation = false;
            resetAllBtns();
            gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// Generates a new Sequence of inputs.
    /// </summary>
    public void generate_New_sequence()
    {
        sequence = new List<int>(){};
        pressed_sequence = new List<int>(){};
        input_sequence = new List<int>(){};

        System.Random r = new System.Random();
        for (int i = 0; i< gm.currentDifficulty * 2; i++)
        {
            int num = r.Next(0,4);
            sequence.Add(num);
        }
    }
    /// <summary>
    /// Animates the buttons according to the generated sequence.
    /// </summary>
    public void trigger_sequence()
    {

        time_counter++;
        {
            if (count < pressed_sequence.Count && time_counter >= 60)
            {
                Lock();
                int sequence_number = sequence[count];
                Btns[sequence_number].animate_btn();

                if (pressed_sequence.Count == sequence.Count+1)
                {
                    CloseMinigame();
                    gm.addToScore(CalculateScore());
                    return;
                }
                count++;
                time_counter = 0;
            }
            if (count >= pressed_sequence.Count)
            {
                HideBlanket();
                in_sequence_animation = false;
                time_counter = 0;
                count = 0;
                WaitForPlayerInput = true;
                
            }
            if (!in_sequence_animation)
            {
                time_counter = 0;
            }
            if (time_counter > 120)
            {
                time_counter = 0;
            }
        }
    }
    /// <summary>
    /// Allows Sequence to be animated.
    /// </summary>
    public void animate_sequence()
    {
        in_sequence_animation = true;
    }
    /// <summary>
    /// Sets the Minigame's Active State to false, and blocks user input for the next time the game Avtivates.
    /// </summary>
    public void CloseMinigame()
    {
        ResetMiniGame();
        gm.Activate_random_event();
    }
    /// <summary>
    /// Start the Sequence animation and blocks player input.
    /// </summary>
    public void startSequence()
    {
        animate_sequence();
        WaitForPlayerInput = true;
        Lock();
    }
    /// <summary>
    /// Checks if the player's input sequence.
    /// </summary>
    /// <returns>Return false if the player's input sequence doesn't match the generated one</returns>
    public bool check_sequence()
    {
        for(int i=0;i<pressed_sequence.Count;i++)
        {
            { 
                if (sequence[i] != input_sequence[i])
                {
                    return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// Displays a list's content on Console.
    /// </summary>
    /// <param name="l">The input to display.</param>
    public void ShowList(List<int> l)
    {
        String s = "";
        foreach(int i in l)
        {
            s = s.Insert(s.Length, "//");
            s = s.Insert(s.Length, i.ToString());
        }
       
    }
   
    /// <summary>
    /// Light's off all of the Buttons.
    /// </summary>
    public void resetAllBtns()
    {
        foreach(ColorBtn btn in Btns)
        {
            btn.light_down();
        }
    }
    /// <summary>
    /// Unlocks the player's input.
    /// </summary>
    public void Unlock()
    {
        blanket.SetActive(false);

    }
    /// <summary>
    /// Unlocks the player's input for 1 second approximitaly.
    /// </summary>
    public void HideBlanket()
    {
        Invoke("Unlock",1.0f);
    }
    /// <summary>
    /// Locks the player's input.
    /// </summary>
    public void Lock()
    {
        blanket.SetActive(true);
    }
    /// <summary>
    /// Locks the player's input for 1 second approximitaly.
    /// </summary>
    public void ShowBlanket()
    {
        Invoke("Lock", 1.0f);
    }
    /// <summary>
    /// Waits for the player's inputs and gives visual feedback.
    /// </summary>
    public void RecordPlayerInputs()
    {
        input_timer++;

        if (pressed_sequence.Count == 0)
        {
            input_timer = pressed_sequence.Count * 180;
        }
        else
        if (input_sequence.Count == pressed_sequence.Count)
        {
            Lock();
            resetAllBtns();
            input_timer = pressed_sequence.Count * 180;
            if (check_sequence())
            {
                Correct();
                GameManager.PlaySound("correct");
            }
            else
            {
                Wrong();
                gm.damage();
            }
                
            input_sequence = new List<int>() { };
            WaitForPlayerInput = false;

        }
        if (input_timer >= pressed_sequence.Count * 180)
        {
            input_timer = 0;
            pressed_sequence.Add(sequence[count]);
            animate_sequence();
            WaitForPlayerInput = false;
            input_sequence = new List<int>() { };

        }
    }
    /// <summary>
    /// Hides the red light ring around the buttons.
    /// </summary>
    private void HideCross()
    {
        Cross.SetActive(false);
    }
    /// <summary>
    /// Shows the red light ring around the buttons, for 1 second approximitaly.
    /// </summary>
    public void Wrong()
    {
        Cross.SetActive(true);
        Invoke("HideCross",1.0f);
    }
    /// <summary>
    /// Hides the green light ring around the buttons.
    /// </summary>
    private void HideCheck()
    {
        Check.SetActive(false);
    }
    /// <summary>
    /// Shows the green light ring around the buttons, for 1 second approximitaly.
    /// </summary>
    private void Correct()
    {
        Check.SetActive(true);
        Invoke("HideCheck",1.0f);
    }
    public override void NewSQS()
    {
        generate_New_sequence();
    }

}
